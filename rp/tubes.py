
import numpy as np


# Data.
# bbox: a D-dimensional bounding box is defined by its two corners (left bottom and right top).
# tube: a tube is a collections of bounding boxes.


TYPE = np.uint16


def get_bbox(*args):
    # Represents a D-dimensional box as a list of coordinates coresponding to
    # its two corners: (x1_low, ..., xD_low, x1_high, ..., xD_high).
    return np.hstack(args).astype(TYPE)


def area(box):
    box = box.astype(np.int64)
    m = len(box) / 2
    return np.prod(np.maximum(0, box[m:] - box[:m]))


def area_tube(tube):
    tube = tube.astype(np.int64)
    return np.prod(np.maximum(0, tube[:, 2:] - tube[:, :2]), axis=1)


def intersection(box_1, box_2):
    m = len(box_1) / 2
    return get_bbox(*np.hstack((
        np.maximum(box_1[:m], box_2[:m]),
        np.minimum(box_1[m:], box_2[m:]))))


def intersection_tube(tube_1, tube_2):
    return np.vstack((
        np.maximum(tube_1[:, 0], tube_2[:, 0]),
        np.maximum(tube_1[:, 1], tube_2[:, 1]),
        np.minimum(tube_1[:, 2], tube_2[:, 2]),
        np.minimum(tube_1[:, 3], tube_2[:, 3]))).T


def union(box_1, box_2):
    if box_2 is None:
        return box_1
    if box_1 is None:
        return box_2
    m = len(box_1) / 2
    return get_bbox(*np.hstack((
        np.minimum(box_1[:m], box_2[:m]),
        np.maximum(box_1[m:], box_2[m:]))))


def compute_mean_iou(tube, gt):

    assert tube.shape == gt.shape, "Groundtruth and the given tube have different shapes."

    n_gt = np.sum(np.sum(gt, axis=1) != 0)

    inter = intersection_tube(tube, gt)

    i_area = area_tube(inter)
    u_area = area_tube(tube) + area_tube(gt) - i_area

    idxs = u_area != 0

    return np.sum(
        i_area[idxs].astype(np.float32) /
        u_area[idxs].astype(np.float32)
    ) / n_gt

