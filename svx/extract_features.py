
"""Generates the commands to extract features for the segmentation algorithm."""


import argparse
import os

from datasets import DATASETS


def change_extension(filename, extension):
    return os.path.splitext(filename)[0] + '.' + extension


def sed_cmd(images, images_path, edges_path, redo=False):

    def get_edge_path(image):
        return os.path.join(edges_path, change_extension(image, 'mat'))

    def chunker(lst, nn):
        for ii in xrange(0, len(lst), nn):
            yield lst[ii: ii + nn]

    if not os.path.exists(edges_path):
        os.makedirs(edges_path)

    edges = [
        (os.path.join(images_path, image), get_edge_path(image))
        for image in images
        if redo or not os.path.exists(get_edge_path(image))]

    for ee in chunker(edges, 50):
        ii, oo = zip(*ee)
        print "matlab -nojvm -nosplash -r \"cd structured_edge_detection; extract_image_edges_batch({'%s'},{'%s'},1,0); exit\"" % (
            "','".join(map(os.path.abspath, ii)),
            "','".join(map(os.path.abspath, oo)))


def ldof_cmd(images_path, images, ii, jj, out):

    im1 = os.tmpnam()
    im2 = os.tmpnam()

    cmd = [
        "convert %s %s.ppm; " % (os.path.join(images_path, images[ii]), im1),
        "convert %s %s.ppm; " % (os.path.join(images_path, images[jj]), im2),
        "./ldof %s.ppm %s.ppm; " % (im1, im2),
        "rm -f %s.ppm %s.ppm %sLDOF.ppm; " % (im1, im2, im1),
        "mv %s %s" % (im1 + 'LDOF.flo', out),
    ]

    return ' '.join(cmd)


def main():

    FEATURES = {'edges', 'flow-forward', 'flow-backward'}

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('--video', help="video to work on.")
    parser.add_argument('-d', '--dataset', choices=DATASETS.keys(), default='ucf_sports', help="the name of the data set.")
    parser.add_argument('-f', '--features', choices=FEATURES, nargs='+', default=FEATURES, help="type of features to used for learning the edge weights.")
    parser.add_argument('-r', '--redo', action='store_true', default=False, help="recompute features even if they already exist.")

    args = parser.parse_args()

    dataset = DATASETS[args.dataset]

    images_path = dataset.get_images_path(args.video)
    edges_path = dataset.get_edges_path(args.video)

    forward_flow_path = dataset.get_flow_path(args.video, 'forward')
    backward_flow_path = dataset.get_flow_path(args.video, 'backward')

    images = [
        im for im in sorted(os.listdir(images_path))
        if im.endswith('.jpg') or im.endswith('.png') or im.endswith('.ppm')]

    if 'edges' in args.features:
        sed_cmd(images, images_path, edges_path, args.redo)

    if 'flow-forward' in args.features and not os.path.exists(forward_flow_path):
        os.makedirs(forward_flow_path)

    if 'flow-backward' in args.features and not os.path.exists(backward_flow_path):
        os.makedirs(backward_flow_path)

    for ii, image in enumerate(images[:-1]):

        if 'flow-forward' in args.features:
            out = os.path.join(forward_flow_path, change_extension(image, 'flo'))
            if args.redo or not os.path.exists(out):
                print ldof_cmd(images_path, images, ii, ii + 1, out)

    for ii, image in enumerate(images[1:], 1):

        if 'flow-backward' in args.features:
            out = os.path.join(backward_flow_path, change_extension(image, 'flo'))
            if not os.path.isfile(out):
                print ldof_cmd(images_path, images, ii, ii - 1, out)


if __name__ == '__main__':
    main()

