#include "numpy_image.h"

/* Compute SLIC superpixels.
*/
int _compute_SLIC( float_layers* lab, int_image* labels, 
                    int superpixelsize, float compactness, bool perturbseeds, 
                    int n_iter, float extend_window,
                    bool output_seeds, float_image* res_out );


int enforce_contiguous_regions( int_image* labels, int_image* res, int K);


/* recompute seeds after enforce_contiguous_regions
   seeds = [(x, y, L, a, b, nb) + optional (Vx, Vy, VL, Va, Vb)]
   mask => which pixels are ok to look at
*/
void _recompute_seeds( float_cube* lab, int_image* labels, float_image* seeds, UBYTE_image* mask );


void draw_SLIC_contours( UBYTE_cube* rgb, int_image* labels );


