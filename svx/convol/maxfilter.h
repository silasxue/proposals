#pragma once
#include "numpy_image.h"


/* * * * * Min/Maximum filter * * * * */


/* compute the 3x3 maximum filter on an image and store the result in <res>
  ori = 'h', 'v', or anything else
*/
void _max_filter_3_B( UBYTE_image* img, char ori, UBYTE_image* res );
void _max_filter_3_i( int_image* img, char ori, int_image* res );
void _max_filter_3_f( float_image* img, char ori, float_image* res );

void _min_filter_3_B( UBYTE_image* img, char ori, UBYTE_image* res );
void _min_filter_3_i( int_image* img, char ori, int_image* res );
void _min_filter_3_f( float_image* img, char ori, float_image* res );


/* Same as above for float_layers* images
  ori = 'h', 'v', or anything else
*/
void _max_filter_3_layers_f( float_layers* img, char ori, float_layers* res );
void _min_filter_3_layers_f( float_layers* img, char ori, float_layers* res );



/* * * * *   Sub-sampling   * * * * */


/* Subsample an array, equivalent to res = img[:,1::2,1::2]
*/
void _subsample2_layers_f( float_layers* img, float_layers* res );



/* * * * * Maximum pooling * * * * */


/* Max-pool in 2x2 px non-overlapping cells
*/
void _maxpool2_layers_f( float_layers* img, float_layers* res );

/* average-pool in 2x2 px non-overlapping cells
*/
void _avgpool2_layers_f( float_layers* img, float_layers* res );
































