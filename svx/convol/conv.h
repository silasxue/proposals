#include "numpy_image.h"


/* * * * * Image smoothing * * * * */


/* Smooth an image with [1,2,1] filter 
   in-place is ok (img can be ==res)
*/
void _smooth_121_B(UBYTE_image* pixels, UBYTE_image* res);
void _smooth_121_f(float_image* pixels, float_image* res);


/* Gaussian filter an image
   in-place is ok (img can be ==res)
*/
void _smooth_gaussian_B( UBYTE_image* img, float sigma, UBYTE_image* res );
void _smooth_gaussian_f( float_image* img, float sigma, float_image* res );


/* Gaussian filter a multi-layers image
   in-place is ok (img can be ==res)
*/
void _smooth_gaussian_layers_B( UBYTE_layers* img, float sigma, UBYTE_layers* res );
void _smooth_gaussian_layers_f( float_layers* img, float sigma, float_layers* res );


/* General convolution with symmetric filters
   in-place is ok (img can be ==res)
*/
void _sym_filter3_f(float_image* pixels, int_Tuple2* filter, float_image* res);
void _sym_filter5_f(float_image* pixels, int_Tuple3* filter, float_image* res);
void _sym_filter7_f(float_image* pixels, int_Tuple4* filter, float_image* res);
























































