#include "graph_dist.h"
#include "std.h"
#include <queue>



/* Build a CSR matrix
*/
void build_csr_from_sorted_rows( const vector<list_node_t>& sorted_rows, const int nc, csr_matrix* csr ) {
  const unsigned int nr = sorted_rows.size();
  
  unsigned int nb=0;
  for(unsigned int l=0; l<nr; l++) 
    nb += sorted_rows[l].size();
  
  csr->nr = nr;
  csr->nc = nc;
  assert(!csr->indptr);
  csr->indptr = NEWA(int, nr+1);
  csr->indices = NEWA(int, nb);
  csr->data = NEWA(float, nb);
  
  unsigned int n=0,r=0;
  for(; r<nr; r++) {
    csr->indptr[r] = n;
    const list_node_t& cols = sorted_rows[r];
    const unsigned int sz = cols.size();
    if(sz)  assert(cols[0]<=cols[sz-1]);
    for(unsigned int i=0; i<sz; i++,n++) {
      csr->indices[n] = cols[i];
      csr->data[n] = 1;
    }
  }
  csr->indptr[r] = n;
  assert(n==nb);
}
void build_csr_from_sorted_rows_weighted( const vector<list_node_dist_t>& sorted_rows, const int nc, csr_matrix* csr ) {
  const unsigned int nr = sorted_rows.size();
  
  unsigned int nb=0;
  for(unsigned int l=0; l<nr; l++) 
    nb += sorted_rows[l].size();
  
  csr->nr = nr;
  csr->nc = nc;
  assert(!csr->indptr);
  csr->indptr = NEWA(int, nr+1);
  csr->indices = NEWA(int, nb);
  csr->data = NEWA(float, nb);
  
  unsigned int n=0,r=0;
  for(; r<nr; r++) {
    csr->indptr[r] = n;
    const list_node_dist_t& cols = sorted_rows[r];
    const unsigned int sz = cols.size();
    if(sz)  assert(cols[0].node<=cols[sz-1].node);
    for(unsigned int i=0; i<sz; i++,n++) {
      csr->indices[n] = cols[i].node;
      csr->data[n] = cols[i].dis;
    }
  }
  csr->indptr[r] = n;
  assert(n==nb);
}



typedef node_dist_t current_t;

template<typename T>
struct smallest_on_top {
  bool operator() (const T& a, const T& b) const {
    return a.dis > b.dis; }
};


/* Find nearest neighbour in a weighted directed graph
   The matrix must be symmetric !
*/
int _find_nn_graph_arr( csr_matrix* graph, int seed, int nmax, int* best, float* dist ) {
  assert(nmax>0);
  assert( graph->nr==graph->nc && (graph->indptr[graph->nr]%2==0) );
  const int* indptr = graph->indptr;
  
  // init done to INF
  float* done = NEWA(float,graph->nr);
  memset(done,0x7F,graph->nr*sizeof(float));
  
  // explore nodes in order of increasing distances
  priority_queue<current_t,vector<current_t>,smallest_on_top<current_t> > stack;
  stack.emplace(seed,0);
  done[seed] = 0;  // mark as done
  
  int n=0;
  while(stack.size()) {
    current_t cur = stack.top();
    stack.pop();
    if(cur.dis > done[cur.node]) continue;
    
    // insert result
    best[n] = cur.node;
    dist[n] = cur.dis;
    n++;
    if( n>= nmax )  break;
    
    // find nearest neighbors
    for(int i=indptr[cur.node]; i<indptr[cur.node+1]; i++) {
      int neigh = graph->indices[i];
      float newd = cur.dis + graph->data[i];
      if( newd>=done[neigh] ) continue;
      
      stack.emplace(neigh, newd); // add only if it makes sense
      done[neigh] = newd;
    }
  }
  
  free(done);
  
  // in case we do not get enough results
  memset(best+n,0xFF,(nmax-n)*sizeof(int));
  memset(dist+n,0x7F,(nmax-n)*sizeof(float));
  return n;
}

int _find_nn_graph( csr_matrix* graph, int seed, int_array* best, float_array* dist ) {
  assert(best->tx==dist->tx);
  return _find_nn_graph_arr( graph, seed, best->tx, best->pixels, dist->pixels );
}


/* Find all nodes inside a ball of given radius
   The matrix must be symmetric !
*/
void _find_dmax_graph_arr( csr_matrix* graph, int seed, float dmax, vector<node_dist_t>& res ) {
  assert(dmax>=0);
  assert( graph->nr==graph->nc && (graph->indptr[graph->nr]%2==0) );
  const int* indptr = graph->indptr;
  
  // init done to INF
  float* done = NEWA(float,graph->nr);
  memset(done,0x7F,graph->nr*sizeof(float));
  
  priority_queue<current_t,vector<current_t>,smallest_on_top<current_t> > stack;
  stack.emplace(seed,0);
  done[seed] = 0;  // mark as done
  
  while(stack.size()) {
    current_t cur = stack.top();
    stack.pop();
    if(cur.dis > dmax)  break;  // cannot improve
    if(cur.dis > done[cur.node]) continue;
    
    // insert result
    res.emplace_back(cur.node,cur.dis);
    
    // find nearest neighbors
    for(int i=indptr[cur.node]; i<indptr[cur.node+1]; i++) {
      int neigh = graph->indices[i];
      float newd = cur.dis + graph->data[i];
      if( newd>=done[neigh] ) continue;
      
      if( newd <= dmax ) { 
        stack.emplace(neigh, newd);
        done[neigh] = newd;
      }
    }
  }
  
  free(done);
}

void find_dmax_graph( csr_matrix* graph, int seed, float dmax, int_array* best, float_array* dist ) {
  vector<node_dist_t> res;
  _find_dmax_graph_arr( graph, seed, dmax, res );
  
  assert(!best->pixels && !dist->pixels);
  best->pixels = NEWA(int,res.size());
  dist->pixels = NEWA(float,res.size());
  best->tx = dist->tx = res.size();
  for(int i=0; i<best->tx; i++) {
    best->pixels[i] = res[i].node;
    dist->pixels[i] = res[i].dis;
  }
}




/* Count the number of neighboring nodes in a given radius (including seed). 
   The matrix must be symmetric !
   If weights!=NULL, then it represents the node weights (instead of 1).
*/
float _count_neighbours_node_arr( const csr_matrix* assoc, int seed, int rad, const float* weights, int* done ) {
  assert( assoc->nr==assoc->nc && (assoc->indptr[assoc->nr]%2==0) );
  assert(rad >= 0 );
  typedef pair<int,int> node_rad_t;
  deque<node_rad_t> stack;
  
  float count = 0;
  stack.emplace_back(seed, rad);
  memset(done,0,assoc->nc*sizeof(int));
  
  while(stack.size()) {
    seed = stack.front().first;
    rad = stack.front().second;
    stack.pop_front();
    if(done[seed])  continue;
    done[seed]++;
    
    if(weights)
      count += weights[seed];
    else
      count++;
    
    if(rad<=0) continue;
    for(int i=assoc->indptr[seed]; i<assoc->indptr[seed+1]; i++) {
      int col = assoc->indices[i];
      if( seed==col || done[col] )  continue;
      stack.emplace_back(col, rad-1);
    }
  }
  
  return count;
}


/* compute the number of neighbours for all graph nodes.
   The matrix must be symmetric !
   If weights!=NULL, then it represents the node weights (instead of 1).
*/
void _count_neighbours( const csr_matrix* assoc, int rad, const float_array* _weights, float_array* res ) {
  const int nm = res->tx;
  assert(assoc->nr==nm && assoc->nc==nm);
  assert(assoc->indptr[nm]%2==0 || !"error: matrix must be square and symmetric");
  assert(!_weights || _weights->tx==nm);
  const float* weights = _weights ? _weights->pixels : NULL;
  
  int* done = NEWA(int,nm);
  
  for(int n=0; n<nm; n++) 
    res->pixels[n] = _count_neighbours_node_arr( assoc, n, rad, weights, done );
  
  free(done);
}



/* Find the neighbours in a given hop radius of all graph nodes. 
   The shortest distance to the seed is returned as well.
   The matrix must be symmetric !
*/
void _find_neighbours_node_arr( const csr_matrix* assoc, int seed, int radmin, int radmax, list_node_t& nghs, char* _done ) 
{
  assert( assoc->nr==assoc->nc && (assoc->indptr[assoc->nr]%2==0) );
  assert(0<=radmin && radmin<=radmax);
  const int* indptr = assoc->indptr;
  
  // init best to INF
  char* done = _done ? _done : NEWA(char,assoc->nr);
  memset(done,0,assoc->nr*sizeof(char));
  
  typedef pair<int,int> node_rad_t;
  deque<node_rad_t> stack; // NOT a priority stack !
  stack.emplace_back(seed,0);
  done[seed] = 1;
  
  while(stack.size()) {
    int node = stack.front().first;
    int rad = stack.front().second;
    stack.pop_front();
    
    // insert result
    if(rad>=radmin)
      nghs.emplace_back(node);
    
    // find nearest neighbors
    if(rad>=radmax)  continue;
    for(int i=indptr[node]; i<indptr[node+1]; i++) {
      int neigh = assoc->indices[i];
      if(!done[neigh]) {
        stack.emplace_back(neigh, rad+1);
        done[neigh] = 1;
      }
    }
  }
  
  if(!_done)  free(done);
}
void _find_neighbours_node( const csr_matrix* assoc, int seed, int radmin, int radmax, int_array* res_out ) {
  list_node_t res;
  _find_neighbours_node_arr( assoc, seed, radmin, radmax, res, NULL );
  
  assert(!res_out->pixels);
  res_out->tx = (int)res.size();
  res_out->pixels = res.data();
  memset(&res,0,sizeof(res)); // steals pointer
}

static int cmp_int ( const void* a, const void* b ) {
  return (*(int*)a) - (*(int*)b);
}
void _find_neighbours( const csr_matrix* assoc, int radmin, int radmax, csr_matrix* res_out, int n_thread ) {
  const int nm = assoc->nr;
  assert((assoc->nc==nm && (assoc->indptr[nm]%2==0)) || !"error: matrix must be square and symmetric");
  
  vector<list_node_t> all_lists(nm);
  
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    char* done = NEWA(char,nm);
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif
    for(int n=0; n<nm; n++) {
      list_node_t& list = all_lists[n];
      _find_neighbours_node_arr( assoc, n, radmin, radmax, list, done );
      qsort( list.data(), list.size(), sizeof(int), cmp_int );
    }
    
    free(done);
  }
  
  build_csr_from_sorted_rows( all_lists, nm, res_out );
}


/* Maximum filter in a radius of 1. Edge weights are not used.
   The matrix must be symmetric !
*/
void _max_filter( const csr_matrix* ngh, float_array* current, float_array* res ) {
  const int nm = ngh->nr;
  assert((ngh->nc==nm && (ngh->indptr[nm]%2==0)) || !"error: matrix must be square and symmetric");
  assert( current->pixels!=res->pixels && current->tx==nm && res->tx==nm);
  const float* curw = current->pixels;
  
//  #ifdef USE_OPENMP
//  #pragma omp parallel for num_threads(n_thread)
//  #endif
  for(int i=0; i<nm; i++) {
    float cur = curw[i];
    for(int j=ngh->indptr[i]; j<ngh->indptr[i+1]; j++) 
      cur = MAX(cur, curw[ngh->indices[j]]);
    res->pixels[i] = cur;
  }
}


/* Find the shortest (weighted) path between two nodes
   graph matrix must be symmetric !
*/
float _shortest_weighted_path( csr_matrix* graph, int seed, int end, int output_path, int_array* path ) {
  const int nr = graph->nr;
  assert(nr==graph->nc);
  const int* indptr = graph->indptr;
  
  // init best to INF
  float* best = NEWA(float,nr);
  memset(best,0x7F,nr*sizeof(float));
  int* prev = NEWA(int, nr );
  memset(prev,0xFF,nr*sizeof(int));
  
  priority_queue<current_t,vector<current_t>,smallest_on_top<current_t> > stack;
  stack.emplace(seed,0);
  best[seed] = 0;  // mark as done
  prev[seed] = -1;
  
  while(stack.size()) {
    current_t cur = stack.top();
    stack.pop();
    if(cur.dis > best[end])  break;  // too late
    if(cur.dis > best[cur.node]) continue;
    
    // find nearest neighbors
    for(int i=indptr[cur.node]; i<indptr[cur.node+1]; i++) {
      int neigh = graph->indices[i];
      float newd = cur.dis + graph->data[i];
      if( newd>=best[end] ) continue; // too late
      
      if( newd < best[neigh] ) { 
        stack.emplace(neigh, newd);
        prev[neigh] = cur.node;
        best[neigh] = newd;
      }
    }
  }
  
  float dis = best[end];
  if( output_path ) {
    int nb, i;
    for(nb=0,i=end; i>=0; nb++)
      i = prev[i];
    
    path->tx = nb;
    path->pixels = NEWA(int, nb);
    for(nb=0,i=end; i>=0; nb++) {
      path->pixels[nb] = i;
      i = prev[i];
    }
  }
  
  free(best);
  free(prev);
  return dis;
}



struct node_rad_dist_t {
  int node, rad;
  float dis;
  node_rad_dist_t(int node, int rad, float dist)
    :node(node),rad(rad), dis(dist){}
} ;



/* Compute the weighted shortest distance from a seed node to all graph nodes 
   within a given number of 'hops'.
   Return an image of minimum distance for each node and for each number of 'hops' : res[node,n_hop-1]
   ngh MUST be a sparse SYMMETRIC matrix
*/
void _shortest_weighted_nhops_node( const csr_matrix* graph, int seed, int nmax_hop, float_image* res ) {
  assert(res->ty==graph->nr && res->tx==nmax_hop);
  assert( graph->nr==graph->nc && (graph->indptr[graph->nr]%2==0) );
  const int* indptr = graph->indptr;
  const int nh = nmax_hop;  // just an alias
  
  // init done to INF
  float* best = res->pixels;
  memset(best,0x7F,res->ty*res->tx*sizeof(float));
  
  deque<node_rad_dist_t> stack;
  stack.emplace_back(seed,0,0);
  memset(best+nh*seed,0,nh*sizeof(float));  // mark seed as done
  
  while(stack.size()) {
    node_rad_dist_t cur = stack.front();
    stack.pop_front();
    
    if(cur.rad >= nmax_hop) continue;
    if(cur.rad && cur.dis>best[nh*cur.node+cur.rad-1]) continue;
    
    // find nearest neighbors
    for(int i=indptr[cur.node]; i<indptr[cur.node+1]; i++) {
      int neigh = graph->indices[i];
      float newd = cur.dis + graph->data[i];
      
      if( newd < best[nh*neigh+cur.rad] ) { // better than existing ?
        stack.emplace_back(neigh, cur.rad+1, newd);
        
        for(int j=cur.rad; j<nh; j++) 
          best[nh*neigh+j] = newd;  // update all distances for j>=cur.rad
      }
    }
  }
}
void _shortest_weighted_nhops( const csr_matrix* graph, int radmax, csr_matrix* res_out, int n_thread ) {
  assert( graph->nr==graph->nc && (graph->indptr[graph->nr]%2==0) );
  assert(radmax>0);
  const int nr = graph->nr;
  
  vector<list_node_dist_t> all_lists(nr);
  
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    float_image dist = {NEWA(float,nr*radmax),radmax,nr};
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif
    for(int n=0; n<nr; n++) {
      if( graph->indptr[n]==graph->indptr[n+1] )  continue; // empty row
      _shortest_weighted_nhops_node( graph, n, radmax, &dist );
      
      // then, report result
      list_node_dist_t& res = all_lists[n];
      for(int i=0; i<nr; i++)
        if( dist.pixels[i*radmax+radmax-1] < 9e9f ) // just look at the best distance for all number of hops<=radmax
          res.emplace_back(i,dist.pixels[i*radmax+radmax-1]);
    }
    
    free(dist.pixels);
  }
  
  build_csr_from_sorted_rows_weighted( all_lists, nr, res_out );
}












































