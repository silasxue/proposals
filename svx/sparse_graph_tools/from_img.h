#include "numpy_image.h"



/* Find neighboring labels and store their relation (number of edge pixels) in a sparse matrix.
    nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat( int nl, int_image* labels, int min_border_size, csr_matrix* res_out );



/* Find neighboring labels, compute their edge cost based on a cost map, 
   and store their relation in a sparse matrix.
     linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
     nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat_weighted( int nl, int_image* labels, float_image* cost_map, char linkage, 
                                    int min_border_size, csr_matrix* res_out );


/* Find neighboring labels, based on euclidean distance between neigboring pixel descriptors,
   and store their relation in a sparse matrix.
     linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
     nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat_euc( int nl, int_image* labels, float_cube* desc, char linkage, 
                               int connec, int min_border_size, csr_matrix* res_out );


/* Find neighboring labels, compute their edge cost based on a cost map, 
   and store their relation in a sparse matrix.
     linkage = 's' (single == mininum) 'a' (average) 'm' (maximum) 'd' (median)
     nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat_weighted_ori( int nl, int_image* labels, float_image* cost_map, float_image* ori_map, 
                                    char linkage, int min_border_size, csr_matrix* res_out );



/* visualize distances between neighboring nodes
*/
void _viz_edge_weights( const csr_matrix* edges, int_image* labels, float_image* _img );



/* Break down a labelled image into connected components.
   Each label == 1 region, which can be further broken down into smaller components.
   
    connec = {4,8}
    min_size = minimum size for a connected components (if larger, connect to any neighbor)
*/
int _label_image_cc( int_image* labels, int connec, int min_size, int_image* res );







