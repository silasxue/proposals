#include "std.h"
#include "colors.h"

/*
http://www.eecs.berkeley.edu/Research/Projects/CS/vision/bsds/code/Util/Lab2RGB.m
*/

void _rgb_to_lab_layers( UBYTE_image3* img, float_layers* res, float color_attenuation ) {
  ASSERT_SAME_SIZE(img,res);
  assert(res->tz==3);
  const int npix = img->tx*img->ty;
  
  const float T=0.008856;
  
  int l;
  #define NSUB 32
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(l=0; l<NSUB; l++) {
    const int start = (l*npix)/NSUB;
    const int end = ((l+1)*npix)/NSUB;
    const int npixsub = end-start;
    UBYTE* img_pix = img->pixels + 3*start;
    float* L_pix = res->pixels + start;
    float* a_pix = L_pix + npix;
    float* b_pix = L_pix + 2*npix;
    int i;
    for(i=0; i<npixsub; i++) {
      float r = *img_pix++/255.f;
      float g = *img_pix++/255.f;
      float b = *img_pix++/255.f;
      
      float X=0.412453 * r + 0.357580 * g + 0.180423 * b;
      float Y=0.212671 * r + 0.715160 * g + 0.072169 * b;
      float Z=0.019334 * r + 0.119193 * g + 0.950227 * b;
      
      X/=0.950456;
      Z/=1.088754;
      
      float Y3 = pow(Y,1./3);
      
      float fX = X>T ? pow(X,1./3) : 7.787 * X + 16/116.;
      float fY = Y>T ? Y3 : 7.787 * Y + 16/116.;
      float fZ = Z>T ? pow(Z,1./3) : 7.787 * Z + 16/116.;

      float L = Y>T ? 116 * Y3 - 16.0 : 903.3 * Y;
      float A = 500 * (fX - fY);
      float B = 200 * (fY - fZ);
      
      if( color_attenuation>0 ) {
        // correct La*b*: dark area or light area have less reliable colors
        float correct_lab = exp(-color_attenuation*pow2(pow2(L/100) - 0.6));
        A *= correct_lab;
        B *= correct_lab;
      }
      
      *L_pix++ = L;
      *a_pix++ = A;
      *b_pix++ = B;
    }
  }
}

void _rgb_to_lab_cube( UBYTE_image3* img, float_cube* res, float color_attenuation ) {
  ASSERT_SAME_SIZE(img,res);
  assert(res->tz==3);
  const int npix = img->tx*img->ty;
  
  const float T=0.008856;
  
  int l;
  #define NSUB 32
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(l=0; l<NSUB; l++) {
    const int start = (l*npix)/NSUB;
    const int end = ((l+1)*npix)/NSUB;
    const int npixsub = end-start;
    UBYTE* img_pix = img->pixels + 3*start;
    float* pres = res->pixels + 3*start;
    int i;
    for(i=0; i<npixsub; i++) {
      float r = *img_pix++/255.f;
      float g = *img_pix++/255.f;
      float b = *img_pix++/255.f;
      
      float X=0.412453 * r + 0.357580 * g + 0.180423 * b;
      float Y=0.212671 * r + 0.715160 * g + 0.072169 * b;
      float Z=0.019334 * r + 0.119193 * g + 0.950227 * b;
      
      X/=0.950456;
      Z/=1.088754;
      
      float Y3 = pow(Y,1./3);
      
      float fX = X>T ? pow(X,1./3) : 7.787 * X + 16/116.;
      float fY = Y>T ? Y3 : 7.787 * Y + 16/116.;
      float fZ = Z>T ? pow(Z,1./3) : 7.787 * Z + 16/116.;

      float L = Y>T ? 116 * Y3 - 16.0 : 903.3 * Y;
      float A = 500 * (fX - fY);
      float B = 200 * (fY - fZ);
      
      if( color_attenuation>0 ) {
        // correct La*b*: dark area or light area have less reliable colors
        float correct_lab = exp(-color_attenuation*pow2(pow2(L/100) - 0.6));
        A *= correct_lab;
        B *= correct_lab;
      }
      
      *pres++ = L;
      *pres++ = A;
      *pres++ = B;
    }
  }
}

static inline UBYTE bound255( float v ) {
  v += 0.5;
  if(v<0) return 0;
  if(v>255) return 255;
  return (UBYTE)v;
}

/*
http://www.eecs.berkeley.edu/Research/Projects/CS/vision/bsds/code/Util/RGB2Lab.m
*/

void _lab_layers_to_rgb( float_layers* img, UBYTE_image3* res ) {
  ASSERT_SAME_SIZE(img,res);
  assert(img->tz==3);
  const int npix = img->tx*img->ty;
  float* L_pix = img->pixels;
  float* a_pix = img->pixels + npix;
  float* b_pix = img->pixels + 2*npix;
  UBYTE* res_pix = res->pixels;
  
  const float T1 = 0.008856;
  const float T2 = 0.206893;
  
  int i;
  for(i=0; i<npix; i++) {
    float L = *L_pix++;
    float A = *a_pix++;
    float B = *b_pix++;
    
    float fY = pow((L + 16) / 116., 3);
    
    int YT = fY>T1;
    
    if(!YT) fY=L / 903.3;
    
    float Y=fY;
    
    /* Alter fY slightly for further calculations */
    
    fY = YT  ? pow(fY,1/3.) : 7.787 * fY + 16/116.;
    
    /* compute X */
    
    float fX = A / 500 + fY;
    int XT = fX > T2;
    
    float X = XT ? pow(fX , 3.) : (fX - 16/116.) / 7.787;
    
    /* Compute Z */
    
    float fZ = fY - B / 200;
    int ZT = fZ > T2;
    float Z = ZT ? pow(fZ , 3) : (fZ - 16/116.) / 7.787;
    
    X*=0.950456;
    Z*=1.088754;
    
    *res_pix++ = bound255(255*(  3.240479 * X -1.537150 * Y -0.498535 * Z ));
    *res_pix++ = bound255(255*( -0.969256 * X +1.875992 * Y +0.041556 * Z ));
    *res_pix++ = bound255(255*(  0.055648 * X -0.204043 * Y +1.057311 * Z ));
  }
}

void _lab_cube_to_rgb( float_cube* img, UBYTE_image3* res ) {
  ASSERT_SAME_SIZE(img,res);
  assert(img->tz==3);
  const int npix = img->tx*img->ty;
  float* pimg = img->pixels;
  UBYTE* res_pix = res->pixels;
  
  const float T1 = 0.008856;
  const float T2 = 0.206893;
  
  int i;
  for(i=0; i<npix; i++) {
    float L = *pimg++;
    float A = *pimg++;
    float B = *pimg++;
    
    float fY = pow((L + 16) / 116., 3);
    
    int YT = fY>T1;
    
    if(!YT) fY=L / 903.3;
    
    float Y=fY;
    
    /* Alter fY slightly for further calculations */
    
    fY = YT  ? pow(fY,1/3.) : 7.787 * fY + 16/116.;
    
    /* compute X */
    
    float fX = A / 500 + fY;
    int XT = fX > T2;
    
    float X = XT ? pow(fX , 3.) : (fX - 16/116.) / 7.787;
    
    /* Compute Z */
    
    float fZ = fY - B / 200;
    int ZT = fZ > T2;
    float Z = ZT ? pow(fZ , 3) : (fZ - 16/116.) / 7.787;
    
    X*=0.950456;
    Z*=1.088754;
    
    *res_pix++ = bound255(255*(  3.240479 * X -1.537150 * Y -0.498535 * Z ));
    *res_pix++ = bound255(255*( -0.969256 * X +1.875992 * Y +0.041556 * Z ));
    *res_pix++ = bound255(255*(  0.055648 * X -0.204043 * Y +1.057311 * Z ));
  }
}

/*
void _average_color_patches( float_layers* img, int patch_size, int extend, float_layers* res  ) {
  assert( img->tz == 3 && res->tz == 3 );
  const int tx = img->tx;
  const int ty = img->ty;
  const int etx = res->tx;
  const int ety = res->ty;
  assert( tx==etx+extend && ety==ty+extend );
  
  // compute a uniform filter for each layer independently
  // the mask is centered in (psize/2,psize/2)
  
  // copy img to res and add black extensions
  float* p = img->pixels;
  float* r = res->pixels;
  int c;
  for(c=0; c<3; c++) {
    int j;
    for(j=0; j<ty; j++) {
      memcpy(r,p,sizeof(float)*tx);
      r += tx;
      p += tx;
      int i;
      for(i=tx; i<etx; i++)
        *r++ = 0;
    }
    for(; j<ety; j++) {
      memset(r,0,sizeof(float)*etx);
      r+= etx;
    }
  }
  
  // color filtering
  uniform_filter( res->pixels+0*etx*ety, etx, ety, patch_size );
  uniform_filter( res->pixels+1*etx*ety, etx, ety, patch_size );
  uniform_filter( res->pixels+2*etx*ety, etx, ety, patch_size );
  
  // normalization
  float* ones = NEWA(float, etx*ety );
  int j;
  for(j=0; j<ety; j++) {
    int i,v= (j<ty? 1 : 0);
    for(i=0; i<tx; i++)
      ones[j*etx+i] = v;
    for(; i<etx; i++)
      ones[j*etx+i] = 0;
  }
  // serves as weight
  uniform_filter( ones, etx, ety, patch_size );
  
  for(c=0; c<3; c++) {
    float* w = ones;
    float* r = res->pixels + c*etx*ety;
    int i;
    for(i=0; i<etx*ety; i++)
      r[i] /= w[i];
  }
  
  free(ones);
} 
*/
























