#ifndef ___STD_H___
#define ___STD_H___

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#define NEWA(type,n) (type*)malloc(sizeof(type)*(n))
#define NEWAC(type,n) (type*)calloc(sizeof(type),(n))
#define NEW(type) NEWA(type,1)
#define REALLOC(ptr,type,n) ptr = (type*)realloc(ptr, sizeof(type)*(n))

#define P(x)  printf(#x " = %g\n",(double)(x));
#define D(x)  P(x)
#define DA(x,nb)  {int _iter; printf(#x " = {"); for(_iter=0; _iter<nb; _iter++) printf("%g,",(double)((x)[_iter])); puts("}");}
#define DA2(x,nr,nc)  {int _iter,_jter; printf(#x " = {\n"); for(_iter=0; _iter<nr; _iter++){for(_jter=0; _jter<nc; _jter++) printf("%g,",(double)(x[_iter][_jter])); printf("}\n");}}
#define DA12(x,tx,ty)  {int _iter,_jter; printf(#x " = {\n"); for(_jter=0; _jter<ty; _jter++){for(_iter=0; _iter<tx; _iter++) printf("%g,",(double)(x[_iter+tx*_jter])); printf("}\n");}}
#define D2(x)     DA(x,2)
#define DP(pt)    printf(#pt " = (%g, %g), scale = %g, ori = %g <--> (%g, %g)\n",(pt)->x,(pt)->y,pt->scale,pt->angle,pt->scale*cos(pt->angle),pt->scale*sin(pt->angle));
#define DPTR(ptr) printf(#ptr " = %p\n",ptr);

#define ASSERT(test,msg,p1)  if(!(test)){fprintf(stderr," ---\n  " msg "\n ---\n",p1); assert(0);}
#define EXIT(msg,p1)         ASSERT(1,msg,p1)


#define MIN(a,b)  (((a)<(b)) ? (a) : (b))
#define MAX(a,b)  (((a)>(b)) ? (a) : (b))
#define SWAP(a,b,type)  {type _t = a; a = b; b = _t;}

#ifdef __cplusplus
static inline float pow2( float f ) {
  return f*f;
}
static inline int pow2i( int f ) {
  return f*f;
}

inline float bound( float m, float v, float M ) {
  if(v<m) return m;
  if(M<v) return M;
  return v;
}

#include <stdint.h>
inline unsigned int fastlog2(const unsigned int x) {
  unsigned int y;
  asm ( "\tbsr %1, %0\n"
      : "=r"(y)
      : "r" (x)
  );
  return y;
}
inline unsigned int upper2(unsigned int x)  {
  if(x<=1)  return 1;
  return 1<<(1+fastlog2(x-1));
}
inline unsigned int upper23(unsigned int x) {
  // return the closest upper number which is of the form 2^x * 3^y
  int r1 = upper2(x) - x;
  int r3 = 3*upper2(1+(x-1)/3) - x;
  int r9 = 9*upper2(1+(x-1)/9) - x;
  int r27=27*upper2(1+(x-1)/27) - x;
  int best = r1;
  if(r3<best) best = r3;
  if(r9<best) best = r9;
  if(r27<best) best = r27;
  best += x;
  return best;
}
inline unsigned int upper235(unsigned int x) {
  // return the closest upper number which is of the form 2^x * 3^y
  int r1 = upper23(x) - x;
  int r5 = 5*upper23(1+(x-1)/5) - x;
  int best = r1;
  if(r5<best) best = r5;
  best += x;
  return best;
}
#else
inline float pow2( float f );
inline unsigned int fastlog2(const unsigned int x);
inline unsigned int upper2(unsigned int x);
inline unsigned int upper23(unsigned int x);
#endif

int fastmedian_i(int *arr, int n, int k);
float fastmedian_f(float *arr, int n, int k);


void check_finite(const char* name, const float* vec, int nb, int do_assert);
#define CHECKFINITE(vec,n)  check_finite(#vec,vec,n,0);
#define ASSERTFINITE(vec,n)  check_finite(#vec,vec,n,1);

const double INF = 1.0/0.0;
const double NaN = 0.0/0.0;

//int isfinite(double d); // already defined somewhere

#include <sys/time.h>
inline double now() 
{
  struct timeval tv;
  gettimeofday (&tv,NULL);
  return (tv.tv_sec*1e3 +tv.tv_usec*1e-3)/1000;
}
#define tic {double t = now();
#define toc t=now()-t; printf("elapsed time = %g ms\n",1000*t);}

// array convenient functions
inline float max_array_f(const float* a, int n) {
  int i=n;
  float res = -1e38f;
  while(i--)  if(a[i]>res)  res=a[i];
  return res;
}
inline int argmax_array_f(const float* a, int n)  {
  int i=n,b=0;
  float res = -1e38f;
  while(i--)  if(a[i]>res)  {res=a[i];b=i;}
  return b;
}
inline int argmin_array_f(const float* a, int n)  {
  int i=n,b=0;
  float res = 1e38f;
  while(i--)  if(a[i]<res)  {res=a[i];b=i;}
  return b;
}
#define max_array_i(a,n)  max_array_i_step(a,n,1)
inline int max_array_i_step(const int* a, int n, int step) {
  int i=n;
  int res = 0x80000000;
  while(i--)  if(a[i*step]>res)  res=a[i*step];
  return res;
}
inline float min_array_f(const float* a, int n) {
  int i=n;
  float res = 1e38f;
  while(i--)  if(a[i]<res)  res=a[i];
  return res;
}
#define min_array_i(a,n)  min_array_i_step(a,n,1)
inline int min_array_i_step(const int* a, int n, int step) {
  int i=n;
  int res = 0x0FFFFFFF;
  while(i--)  if(a[i*step]<res)  res=a[i*step];
  return res;
}
inline float sum_array_f(const float* a, int n) {
  int i=n;
  double res = 0;
  while(i--)  res+=a[i];
  return (float)res;
}
inline int sum_array_i(const int* a, int n) {
  int i=n;
  int res = 0;
  while(i--)  res+=a[i];
  return res;
}
inline float mean_array_f(const float* a, int n) {
  return sum_array_f(a,n)/n;
}
inline double mean_array_i(const int* a, int n) {
  return sum_array_i(a,n)/(double)n;
}

inline int randint(int nmax) {
  assert(nmax<=RAND_MAX/8);
  return rand() % nmax;
}

#endif
























